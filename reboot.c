#include <unistd.h>
#include <linux/reboot.h>
#include <sys/reboot.h>

void rebootsystem(){
	sync();
	reboot(LINUX_REBOOT_CMD_RESTART);
}
